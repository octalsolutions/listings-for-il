# README #

This project is to provide syntax highlighting for Intermediate Language (.NET) so that IL code looks nice in the generated PDF.

So instead of this:
![Zrzut ekranu 2016-10-30 o 14.10.47.png](https://bitbucket.org/repo/bELpdn/images/3917657220-Zrzut%20ekranu%202016-10-30%20o%2014.10.47.png)

you get this:
![Zrzut ekranu 2016-10-30 o 14.08.30.png](https://bitbucket.org/repo/bELpdn/images/1740994266-Zrzut%20ekranu%202016-10-30%20o%2014.08.30.png)

### Contribution guidelines ###

* Please contribute as this is an ongoing project

### Who do I talk to? ###

* Repo owner